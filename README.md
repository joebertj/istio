# istio

Demo of Istio Features

## kenchlightyear.com
![kenchlightyear.com](auth/public/kl.png "kenchlightyear.com")

## Installation

### Store Github token into Kubernetes Secret
Will be used by Tekton to clone project used for CICD
```
SSH_PRIVATE_KEY=$(cat ~/.ssh/id_rsa2 | base64 -w 0)
KNOWN_HOSTS=$(ssh-keyscan github.com | base64 -w 0)
sed -i "s/ssh-privatekey:.*/ssh-privatekey: $SSH_PRIVATE_KEY/" git-secret.yaml
sed -i "s/known_hosts:.*/known_hosts: $KNOWN_HOSTS/" git-secret.yaml
kubectl apply -f git-secret.yaml
sed -i "s/ssh-privatekey:.*/ssh-privatekey: /" git-secret.yaml
sed -i "s/known_hosts:.*/known_hosts: /" git-secret.yaml
```

### Install Tekton
```
wget -O tekton.yaml https://github.com/tektoncd/pipeline/releases/download/v0.8.0/release.yaml
kubectl apply -f tekton.yaml
kubectl apply -f tekton-service-account.yaml
wget -O tekton-dashboard.yaml https://github.com/tektoncd/dashboard/releases/download/v0.2.1/dashboard-latest-release.yaml
kubectl apply -f tekton-dashboard.yaml
kubectl delete task -n tekton-pipelines pipeline0-task
kubectl delete pipeline -n tekton-pipelines pipeline0
```

### Install Helm and Tiller  
```
wget -O helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get
bash helm.sh
kubectl -n kube-system create serviceaccount tiller
kubectl create clusterrolebinding tiller --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --wait --service-account tiller
helm repo update
```	

### Configure Grafana Secret
```
GRAFANAPASS=`head -1 grafana-secret | tr -d '\n' | base64`
sed -i "s/passphrase:.*/passphrase: $GRAFANAPASS/" grafana-secret.yaml
kubectl apply -f grafana-secret.yaml
sed -i "s/passphrase:.*/passphrase: /" grafana-secret.yaml
```

### Configure Kiali Secret
```
KIALIPASS=`head -1 kiali-secret | tr -d '\n' | base64`
sed -i "s/passphrase:.*/passphrase: $KIALIPASS/" kiali-secret.yaml
kubectl apply -f kiali-secret.yaml
sed -i "s/passphrase:.*/passphrase: /" kiali-secret.yaml
```

### Configure Cloudflare Secret
```
CFPASS=`head -1 cloudflare-secret | tr -d '\n' | base64`
sed -i "s/api-key\.txt:.*/api-key\.txt: $CFPASS/" cloudflare-secret.yaml
kubectl apply -f cloudflare-secret.yaml
sed -i "s/api-key\.txt:.*/api-key\.txt: /" cloudflare-secret.yaml
```

### Configure Route 53 Secret
Secret shoud be on the same namespace as cert-manager
```
R53PASS=`head -1 route53-secret | tr -d '\n' | base64`
sed -i "s/secret-access-key:.*/secret-access-key: $R53PASS/" route53-secret.yaml
kubectl apply -f route53-secret.yaml
sed -i "s/secret-access-key:.*/secret-access-key: /" route53-secret.yaml
```

### Install Istio
Letsencrypt will refuse connection from cert-manager older than 0.8.0. Secrets are searched on kube-system or istio-system namespaces. 
Cert-manager version up to 0.10.0 uses API version certmanager.k8s.io/v1alpha1 while onwards is cert-manager.io/v1alpha2. Also the secrets by default are in cert-manager namespace. 
```
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.4.5 sh -
cd istio-1.4.5
sudo mv bin/istioctl /usr/local/bin
helm template install/kubernetes/helm/istio-init --name istio-init --namespace istio-system --set certmanager.enabled=true | kubectl apply -f -
helm template install/kubernetes/helm/istio --name istio --namespace istio-system \
  --set certmanager.enabled=true \
  --set certmanager.email=joebertj@kenchlightyear.com \
  --set gateways.istio-ingressgateway.sds.enabled=true \
  --set global.k8sIngress.enabled=false \
  --set grafana.enabled=true \
  --set grafana.persist=true \
  --set grafana.accessMode=ReadWriteOnce \
  --set grafana.security.enabled=true \
  --set kiali.enabled=true \
  --set kiali.contextPath=/ \
  --set kiali.dashboard.grafanaURL=http://grafana.kenchlightyear.cloud/ \
  --set kiali.dashboard.jaegerURL=http://jaeger.kenchlightyear.cloud/ \
  --set servicegraph.enabled=true \
  --set tracing.enabled=true \
  --set tracing.jaeger.persist=true \
  --set tracing.jaeger.accessMode=ReadWriteOnce \
  | kubectl replace -f -
kubectl label namespace default istio-injection=enabled
cd ..
```
#### kubectl set image deploy certmanager -n istio-system certmanager=quay.io/jetstack/cert-manager-controller:v0.8.0
####  --set global.k8sIngress.enableHttps=true \
####  --set global.k8sIngress.gatewayName=ingressgateway \


#### Setup Domain Pre-requisites
This to configure the certificate issuer using DNS provider API of Cloudflare. The certmanager application of istio will first issue a request to Cloudflare to prepare records for Letsencrypt.
![certmanager](cert.png "Cert manager")
```
kubectl apply -f cloudflare-secret.yaml
kubectl replace -f cluster-issuer.yaml
kubectl apply -f kenchlightyear-certificate.yaml

```

### Install Knative Serving and Eventing
Required by Knative Application GitHook
```
wget https://github.com/knative/serving/releases/download/v0.8.1/serving.yaml
wget -O eventing.yaml https://github.com/knative/eventing/releases/download/v0.8.1/release.yaml
kubectl apply --selector knative.dev/crd-install=true --filename serving.yaml --filename eventing.yaml
kubectl apply --filename serving.yaml --filename eventing.yaml
```

### Apply domain
`kubectl apply -f domain.yaml`

### Install GitHook
This is the Knative application that will receive the webhook
```
wget -O githook.yaml https://gitlab.com/pongsatt/githook/-/jobs/347255776/artifacts/raw/release.yaml
kubectl apply -f githook.yaml
```

## Build and deploy the auth application
```
cd auth
docker build -t asia.gcr.io/container-254002/auth .
docker push asia.gcr.io/container-254002/auth
kubectl apply -f auth.yaml
kubectl apply -f auth-gateway.yaml
kubectl apply -f auth-virtualservice.yaml
kubectl apply -f auth-policy.yaml 
cd ..
```

## Build and deploy the jaeger-php application
```
cd auth
docker build -t asia.gcr.io/container-254002/jaeger-php .
docker push asia.gcr.io/container-254002/jaeger-php
kubectl apply -f jaeger-php.yaml
kubectl apply -f jaeger-php-virtualservice.yaml
cd ..
```

## Troubleshooting
`kubectl run tmp-shell --rm -i --tty --image nicolaka/netshoot --generator=run-pod/v1 -- /bin/bash`
