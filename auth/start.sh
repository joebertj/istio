#!/bin/sh

echo "Clearing any old processes..."
rm -f /var/run/apache2/httpd.pid

echo "Starting php-fpm..."
php-fpm -D

echo "Starting apache..."
httpd -D FOREGROUND
