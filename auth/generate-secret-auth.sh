#!/bin/bash

cp auth-secret.yaml auth0.yaml
sed -i "s;__ID__;$(grep AUTH0_CLIENT_ID .env | awk -F'=' '{print $2}' | tr -d '\n' | base64 -w 0);" auth0.yaml
sed -i "s;__DOMAIN__;$(grep AUTH0_DOMAIN .env | awk -F'=' '{print $2}' | tr -d '\n' | base64 -w 0);" auth0.yaml
sed -i "s;__SECRET__;$(grep AUTH0_CLIENT_SECRET .env | awk -F'=' '{print $2}' | tr -d '\n' | base64 -w 0);" auth0.yaml
sed -i "s;__CALLBACK__;$(grep AUTH0_CALLBACK_URL .env | awk -F'=' '{print $2}' | tr -d '\n' | base64 -w 0);" auth0.yaml
sed -i "s;__AUDIENCE__;$(grep AUTH0_AUDIENCE .env | awk -F'=' '{print $2}' | tr -d '\n' | base64 -w 0);" auth0.yaml
kubectl apply -f auth0.yaml
