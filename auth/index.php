<?php
  require __DIR__ . '/vendor/autoload.php';

  require __DIR__ . '/dotenv-loader.php';

  $domain        = getenv('AUTH0_DOMAIN');
  $client_id     = getenv('AUTH0_CLIENT_ID');
  $client_secret = getenv('AUTH0_CLIENT_SECRET');
  $redirect_uri  = getenv('AUTH0_CALLBACK_URL');
  $audience      = getenv('AUTH0_AUDIENCE');
  session_start();
?>  
<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.1.0.min.js" type="text/javascript"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- font awesome from BootstrapCDN -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

        <link href="public/app.css" rel="stylesheet">



    </head>
    <body class="home">
        <div class="container">
            <div class="login-page clearfix">
<?php 
if (!isset($_GET['code'])) {
    // Check if we need to show the "Sign In" link
    $params = array (
      'audience' => $audience,
      'scope' => 'openid profile read:traces',
      'response_type' => 'code',
      'client_id' => $client_id,
      'state' => generateRandomString(),
      'redirect_uri' => $redirect_uri
    );
 
    $_SESSION['oauth2state']=$params['state'];
    $str_params = '';
    foreach($params as $key=>$value) {
      $str_params .= $key . "=" . urlencode($value) . "&";
    }
    ?>
	      <div class="login-box auth0-box before">
                <img src="public/kl.png" />
                <h3>Istio Auth0 Demo</h3>
                <p>Zero friction identity infrastructure, built for developers v0.4</p>
                <a id="qsLoginBtn" class="btn btn-primary btn-lg btn-login btn-block" href="https://<?php echo $domain;?>/authorize?<?php echo $str_params;?>">Sign In</a>
                <img src="public/auth.png" />
              </div> 
<?php
} elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {
    // If the "state" var is present in the $_GET, let's validate it
    if (isset($_SESSION['oauth2state'])) {
        unset($_SESSION['oauth2state']);
    }
     
    exit('Invalid state');
 
} elseif(isset($_GET['code']) && !empty($_GET['code'])) {
    // If the auth "code" is present in the $_GET
    // let's exchange it for the access token
    $params = array (
      'grant_type' => 'authorization_code',
      'client_id' => $client_id,
      'client_secret' => $client_secret,
      'code' => $_GET['code'],
      'redirect_uri' => $redirect_uri
    );
 
    $str_params = '';
    foreach($params as $key=>$value) {
      $str_params .= $key . "=" . urlencode($value) . "&";
    }
 
    $curl = curl_init();
 
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://$domain/oauth/token",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $str_params
    ));
 
    $curl_response = curl_exec($curl);
    $curl_error = curl_error($curl);
 
    curl_close($curl);
 
    if ($curl_error) {
      echo "Error in the CURL response:" . $curl_error;
    } else {
      $arr_json_data = json_decode($curl_response);
 
      if (isset($arr_json_data->access_token)) {
	    $access_token = $arr_json_data->access_token;
?>
	    <script>
    		function copy(text) {
      		    var t = document.getElementById('t');
      		    t.innerHTML = text;
      		    t.select();
      		    try {
        		var successful = document.execCommand('copy');
        		var msg = successful ? 'successfully' : 'unsuccessfully';
        		console.log('text coppied ' + msg);
      		    } catch (err) {
        		console.log('Unable to copy text');
      		    }
      		    t.innerHTML = '';
		}
	        function curl(url, token) {
		    var req = new XMLHttpRequest();
      		    var t = document.getElementById('t');
		    req.open('GET', url, true);
		    req.onreadystatechange = function (aEvt) {
  			if (req.readyState == 4) {
			    if(req.status == 200)
				t.innerHTML = req.responseText.replace(/<\/?[^>]+(>|$)/g, "");
     			    else
      				alert("Error loading page\n");
  			}
		    };
		req.setRequestHeader('Authorization', 'Bearer ' + token);
		req.send();
		}
  	    </script>
<?php
        $curl = curl_init();
 
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://$domain/userinfo",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $access_token"
          )
        ));
 
	$curl_response = curl_exec($curl);
        $curl_error = curl_error($curl);
 
        curl_close($curl);
 
        if ($curl_error) {
          echo "Error in the CURL response from DEMO API:" . $curl_error;
	} else {
	  /*
	   * $arr_json_data = json_decode('{
	      "sub": "info@kenchlightyear.com",
	      "picture": "public/kl.png",
  	      "nickname": "auth"	  
	      }');
	   */ 
          $arr_json_data = json_decode($curl_response);
	  
	  if (isset($arr_json_data->sub)) {
              $picture = $arr_json_data->picture;		
              $nickname = $arr_json_data->nickname;		
?>
	      <div class="logged-in-box auth0-box logged-in">
                <h1 id="logo"><img src="public/kl.png" /></h1>
                <h2>Welcome <span class="nickname"><?php echo $nickname ?></span></h2>
                <img class="avatar" src="<?php echo $picture ?>"/>
              </div>
	      <div class="container">
                <a class="btn btn-danger" onclick="curl('https://auth.kenchlightyear.cloud/app','<?php echo $access_token;?>')">App Async</a>
  	    	<a class="btn btn-info" onclick="copy('<?php echo $access_token;?>')">Copy Access Token</a>
                <a class="btn btn-danger" href="/app" target="_blank">Protected App</a>
                <a id="qsLogoutBtn" class="btn btn-warning btn-logout" href="/logout.php">Logout</a>
              </div>
  	      <textarea id=t></textarea>
<?php
	  } else {
              echo 'Invalid response, no sub was found.' . $curl_response;
          }
	  //echo "Demo API Response:" . $curl_response;
        }
      } else {
        echo 'Invalid response, no access token was found.' . $curl_response;
      }
    }
}
function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}
?>
            </div>
        </div>
    </body>
</html>
